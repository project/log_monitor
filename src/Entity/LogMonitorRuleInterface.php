<?php

namespace Drupal\log_monitor\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Log monitor rule entities.
 */
interface LogMonitorRuleInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
