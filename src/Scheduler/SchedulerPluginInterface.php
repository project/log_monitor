<?php

namespace Drupal\log_monitor\Scheduler;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines an interface for Scheduler plugin plugins.
 */
interface SchedulerPluginInterface extends PluginInspectionInterface {

}
