<?php

namespace Drupal\log_monitor\Plugin\log_monitor\Formatter;

use Drupal\Component\Plugin\PluginBase;
use Drupal\log_monitor\Formatter\FormatterPluginInterface;

/**
 * Base class for Formatter plugin plugins.
 */
abstract class FormatterPluginBase extends PluginBase implements FormatterPluginInterface {


  // Add common methods and abstract methods for your plugin type here.

}
